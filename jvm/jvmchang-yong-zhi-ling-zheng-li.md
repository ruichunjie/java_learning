# JVM常用指令整理

* -XX: -/+UseTLAB   虚拟机是否使用TLAB
* +XX:CompactField   默认为true, 对象实例数据中, 子类较窄的变量也允许插入到父类变量的空隙中，以节省内存空间

* -XX: FieldsAllocationStyle  默认为1， 实例对象中有效信息的存储顺序

```java
      0  先放入oops, 在放入基本变量
      1  先放如基本变量， 再放oop 
      2  oop 和基本变量交叉存储
```

* -XX:+UseCompressedOops 指针压缩
* -XX:+HeapDumpOnOutOFMemoryError 内存溢出时Dump当前内存堆转出快照
* -Xss 栈容量参数设定，栈不支持扩展

* -XX:MaxMetaSpaceSize  元空间最大值，默认-1， 不限制
* -XX: Metaspace: 指定元空间初始空间大小，字节为单位， 达到后触发垃圾收集，进行类型卸载， 收集器对该值进行了调整， 如果释放了大量的空间， 适当降低该值，如果释放了很少的空间， 适当提高该值
* -XX: MinMetaspaceFreeRatio: 垃圾收集后控制最小的元空间剩余容量百分比， 减少因为元空间不足导致的垃圾收集频率。
* -XX:MaxMetaspcaceFreeRatio: 控制最大的元空间剩余容量百分比

* -XX: MaxDirectMemorySize 直接内存的容量大小， 默认Java堆最大值 -Xmx