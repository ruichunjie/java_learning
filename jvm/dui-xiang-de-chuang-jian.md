 

[TOC]

# 对象地创建



## 类加载

​     *当Java虚拟机遇到一条字节码new指令时， 首先去检查这个指令的参数是否能在常量池中定位到一个类的符号引用，并且检查这个符号引用代表的类是否已被加载， 解析和初始化， 如果没有，必须先执行相应的类加载过程*

## 分配内存

   *分配内存有两种方式，分别是指针碰撞， 空闲列表， 选择哪种分配方式由Java堆是否规整有关， Java堆是否规整是由所采用的垃圾收集器是否带有空间压缩整理的能力决定。 当使用Serial, ParNew带压缩整理的收集器时， 系统采用的是指针碰撞， 如果是采用CMS清楚算法收集器， 理论上只能采用空闲列表来分配*
   此处之所以说理论上，是因为在CMS的实现里，为了多数能分配的更快， 设计了一个Linear Allocation Buffer 分配缓冲区额， 通过空闲列表拿到一大块分配缓冲区后， 里面可以使用指针碰撞分配。
   另一个问题在于分配内存很频繁， 并发线程不安全， 解决问题有两种， 一种是进行同步处理，也就是CAS配合失败重试保证更新原子性， 另一种是按线程分在不同空间，也就是TLAB， 可以通过-XX: +/-UseTLAB来设定
    

## 对象设置

内存分配完成后， 虚拟机将分配到的内存空间(不包括对象头)初始化为零值， 如果使用TLAB，也可以提前到TLAB分配时上进行
接下来要对对象进行必要设置，比如这个对象是哪个类的实例， 如何找到元数据信息，哈希码， GC分代年龄等

## 构造函数

new之后， 会跟随invokespecial 指令(不通过new 不一定如此)， Class<init>方法执行, 对象就这完全构造出来了

# 对象的内存布局

简单可以分为三个部分: 对象头， 实例数据, 对齐填充

## 对象头

对象头包括两类信息， 一类是存储对象自身的运行时数据， 如哈希码， GC分代年龄， 锁状态标识， 线程持有锁， 偏向线程ID，偏向时间戳等， 长度在32位和64位中分别为32比特和64比特， 也就是"Mark Word"。另一部分是类型指针， 对象指向它的类型元数据的指针， 如果对象是Java数组， 对象头中还有一块记录数组长度的数据。

## 实例数据

实例数据是对象存储的有效信息， 存储的顺序收到虚拟机参数(-XX:FieldsAllocationStyle)和对象在源码中的定义顺序的影响, 默认 longs/doubles, ints, shorts/chars, bytes/booleans, oops, 相同宽度的字段总是分配到一起， 父类定义的变量在子类前面， 如果+XX:CompactField参数为true, 默认为true, 子类较窄的变量也允许插入到父类变量的空隙中，以节省内存空间

## 对齐填充

占位符， 整个对象必须是8的整数倍

# 访问定位

句柄访问， 直接指针访问， HotSpot 直接指针访问， Shenandoah会有一次额外转发

