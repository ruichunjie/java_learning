## Atomic容器

##### AtomicInteger和AtomicLong

##### AtomicBoolean和AtomicReference

##### AtomicStampedReference和AtomicMarkableReference

##### AtomicIntegerFieldUpdater， AtomicLongFieldUpdater, AtomicReferenceFieldUpdate

这个的成员变量必须是int， 而且要用volatile修饰

##### AtomicIntegerArray

##### Striped64 LongAddr LongAccumulator

## BlockingQueue

### ArrayBlockingQueue

数组实现的环形队列， 核心数据结构是takeIndex, putIndex, lock , notEmpty, notFull

### LinkedBlockingQueue

基于单向链表的阻塞队列， 核心元素是原子变量count, 链表头部head, 链表尾部last, takeLock, notEmpty,putLock, notFull

#### 和ArrayBlockingQueue区别

-   两把锁， put和put之间 take和take之间互斥， put和take不互斥， 因为count双方都要操作， 要是原子类
-   因为各自拿了一把锁， 需要调用对方signal时， 要加上对方的锁， signalNotEmpty()和signalNotFull()

### PriorityBlockingQueue

按照元素优先级比较大小， 核心是用数组实现的二叉小根堆queue数组， 一个锁和notEmpty条件，如果不指定大小，默认11，超过大小后自动扩容

### DelayQueue

按照延迟时间从小到大的PriorityQueue， 核心是一个lock锁， available的condition， PriorityQueue 的q

### SynchronizedQueue

本身没有容量，用链表实现, 公平非公平 TransferQueue TransferStack

## BlockingDeque

### LinkedBlockingDeque

BlockingDeque只有LikedblockingDeque一个实现， 用了一个锁和notFull, notEmpty两个Condition

## CopyOnWrite

### CopyOnWriteArrayList

读的时候不加锁， 添加的时候拷贝一份并且使用synchronized(lock)

### CopyOnWriteArraySet

内部封装了一个CopyOnWriteArrayList