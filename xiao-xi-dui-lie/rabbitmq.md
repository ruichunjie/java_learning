## RabitMQ安装
### 单机安装
##### 安装依赖
官网:   
 https://www.rabbitmq.com/install-rpm.html  
 http://www.rabbitmq.com/which-erlang.html   
 https://github.com/rabbitmq/erlang-rpm/releases
```shell
  sudo rpm -Uvh erlang-23.3.3-1.el7.x86_64.rpm 
  yum install socat -y
  sudo rpm -Uvh rabbitmq-server-3.8.15-1.el7.noarch.rpm 
  systemctl start rabbitmq-server
  systemctl enable  rabbitmq-server
  systemctl status rabbitmq-server
  systemctl stop rabbimq-server
```
##### 安装插件
这里主要说三个插件，一个是页面管理插件managerment plugin, 另一个是监控插件rabbitmq_prometheus， 最后一个插件是延迟消息插件rabbitmq_delayed_message_exchange. 延迟消息插件需要下载， 官网地址是https://www.rabbitmq.com/community-plugins.html, 下载后放到/usr/lib/rabbitmq/lib/rabbitmq_server-3.8.15/plugins下。
 ```shell
   rabbitmq-plugins enable rabbitmq_management
   rabbitmq-plugins enable rabbitmq_prometheus
   rabbitmq-plugins enable rabbitmq_delayed_message_exchange
   ## 查看
   cat /etc/rabbitmq/enabled_plugins
   rabbitmq-plugins list
 ```
 
##### 防火墙
 RabbitMQ需要开放一些端口，一般有4369,5672,25672,15672,官网地址是https://www.rabbitmq.com/networking.html,
 ```shell
   firewall-cmd --zone=public --add-port=5672/tcp --permanent
   firewall-cmd --reload
   firewall-cmd --query-port=5672/tcp
 ```
 
##### 设置用户及权限
```shell
  rabbitmqctl add_user admin admin
  rabbitmqctl set_user_tags admin administrator
  rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
```

> none

不能访问managerment plugin

> management

用户可以通过AMQP做的任何事外加

* 列出自己可以通过AMQP登入的virtual hosts
* 查看自己的virtual  hosts中的queues, exchanges和bindings
* 查看和关闭自己的channels和connections
* 查看有关自己的virtual hosts的全局统计信息，包含其他用户在这些virtual hosts中的活动

> policymaker

* management做的任何事
* 查看, 创建和删除自己的virtual hosts所属的policies和parameters

> monitoring

* management做的任何事
* 列出所有virtual hosts， 包括他们不能登录的virtual hosts
* 查看其他用户的connections和channels
* 查看节点级别数据, 如clustering和memory使用情况
* 查看真正关于virtual hosts全局统计信息

> administrator

* policymaker做的任何事
* monitor做的任何事
* 查看创建和删除virtual hosts
* 查看创建和删除users
* 查看创建和删除 permissions
* 查看创建和删除connections

https://www.rabbitmq.com/management.html
##### 配置文件
默认情况下启动RabbitMQ时没有配置文件，如果要配置的话， 可以添加/etc/rabbitmq/rabbitmq.conf文件， 具体参数和信息参考
https://github.com/rabbitmq/rabbitmq-server/blob/master/deps/rabbit/docs/rabbitmq.conf.example,
https://www.rabbitmq.com/configure.html#config-items

### 集群部署
#### 多机多节点集群部署
##### 安装依赖
准备三台安装好RabbitMQ的机器， 参考单机安装
##### 修改配置文件
```shell
   vim /etc/hosts
   10.10.1.12 node1
   10.10.1.13 node2
   10.10.1.14 node3
   ## 将文件发送到node2 node3
   scp /etc/hosts  root@node2:/etc/
   scp /var/lib/rabitmq/.erlang.cookie root@node2:/var/lib/rabbitmq/
   ## 设置hostname
   hostname node1
```
##### 开放对应的端口，启动RabbitMQ
```shell
   systemctl start rabbitmq-server 或者 rabbitmq-server -detached
```
加入集群
```shell
   rabbitmqctl stop_app
   rabbitmqctl reset
   rabbitmqctl join_cluster rabbit@node1 --ram
   rabbitmqctl start_app
```
查看集群状态
```shell
    rabbitmqctl cluster_status
```

常用命令集合
```shell
rabbitmq-server
rabbitmq-server -detached
rabtitmq-server stop
rabbitmqctl list_queues
rabbitmqctl list_vhosts
rabbitmqctl start_app
rabbitmqctl stop_app
rabbitmqctl status
rabbitmq-plugins list
rabbitmqctl add_user username password
rabbitmqctl list_users
rabbitmqctl delete user
rabbitmqctl clear_user_permissions username
rabbitmqctl clear_permissions -p vhostpath username
rabbitmqctl list_user_permissions username
rabbitmqctl change_password username password
rabbitmqctl set_permissions -p vhostpath username ".*" ".*" ".*"
rabbitmqctl add_vhost
rabbitmqctl list_hosts
rabbitmqctl list_permissions -p  vhostpath
rabbitmqctl delete_vhost vhostpath
rabbitmqctl reset
```

#### 单机多节点部署
已经安装RabbitMQ 的机器，参考单机安装
##### 添加端口， 重启防火墙
##### 启动RabbitMq
在/etc/rabbitmq/rabbimq-env.conf增加以下内容
```shell
    RABBITMQ_NODE_PORT=5672
    RABBITMQ_NODENAME=rabbit1
```
```shell
  rabbimq-server -detached
  RABBITMQ_NODE_PORT=5673 RABBITMQ_SERVER_START_ARGS="-rabbitmq_management listener[{port, 15673}]"  RABBITMQ_NODENAME=rabbit2 rabbitmq-server -detached
  RABBITMQ_NODE_PORT=5674 RABBITMQ_SERVER_START_ARGS="-rabbitmq_management listener[{port, 15674}]"  RABBITMQ_NODENAME=rabbit4 rabbitmq-server -detached
```
```shell
   rabbitmqctl -n rabbit2 stop_app
   rabbitmqctl -n rabbit2 reset
   rabbitmqctl -n rabbit2 join_cluster rabbit1 --ram
   rabbitmqctl -n rabbit2 start_app
   rabbitmqctl cluster_status
   rabbitmqctl -n rabbit3 stop_app
   rabbitmqctl -n rabbit forget_cluster_node rabbit3
   rabbitmqctl set_cluster_name abc
```

普通模式,  只是同步了元数据，并没有同步队列内容, 存在节点故障问题

元数据包含: 
* 队列元数据(队列的名称和属性) 
* 交换器(交换器的名称和属性)
* 绑定关系元数据(交换器与队列或者交换器和交换器)
* vhost元数据:为队列，交换器，和绑定提供命名空间及安全属性之间的绑定关系

#### 镜像队列
官网: https://www.rabbitmq.com/ha.html#mirroring-arguments
管理界面->Admin->Policies-> Add/update a policy
拷贝了队列内容， 数据冗余
```shell
name: ha-all
Pattern: ^
Apply to:Queues
Priority: 0
Definition: ha-mode=all
```
或者在三台机器上执行命令
./rabbitmqctl set_policy ha-all "^" '{"ha-mode":"all"}'

| ha-mode | ha-params  |
| ------- | ---------- |
| exactly | count      |
| all     | none       |
| nodes   | node names |

#### HAProxy
```shell
yum install gcc -y
tar -zxf haproxy-2.1.0.tar.gz
cd haproxy-2.1.0
make TARGET=linux-glibc
make install
mkdir /etc/haproxy
groupadd -r -g 149 haproxy
useradd -g haproxy -r -s /sbin/nologin -u 149 haproxy
touch /etc/haproxy/haproxy.cfg
yum -y install haproxy
```

```bash
global
  log 127.0.0.1 local0 info
  #服务器最大并发连接数
  maxconn 5120
  # chroot /tmp
  #指定用户
  uid 149
  #指定组
  gid 149
  daemon
  quiet
  ## 指定启动的haproxy进程个数， 只能用于守护进程模式的haproxy， 默认启动一个
  nbprox 1
  pidfile /var/run/haproxy.pid
  
defaults
   log global
   # tcp 实例运行在tcp模式， 第4层代理
   mode tcp
   option tcplog
   option dontlognull
   retries 3
   option redispath
   maxconn 2000
   timeout connect 5s
   timeout client 60000
   timeout server 15000
   
listen rabbitmq_cluster
   bind 192.168.100.101:5672
   mode tcp
   balance roundrobin
   server rabbitmqNode1 192.168.100:102:5672 inner  5000 rise 2 fail 2
   server rabbitmqNode2 192.168.100:102:5673 inner  5000 rise 2 fail 2

# 监控
listen stats
   bind 192.168.100.101:9000
   mode http
   option httplog
   stats enable
   stats uri /rabbitmq-stats
   stats refresh 5s
```

```shell
haproxy -f /etc/haproxy/haproxy.cfg
```