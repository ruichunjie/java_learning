``AMQP全称全称高级消息队列协议(Advanced Message Queuing Protocol), 类似JMS， 兼容JMS协议, 属于传输层协议``

### 组件架构

* Server/Broker: 服务实例
* Virtual host: 虚拟主机
* Exchange: 交换器， 接收Proceducer发来的消息，并转发到对应的Message Queue
* Message Queue: 实际存储消息的容器
* Publisher: 消息发送者
* Consumer: 消息消费者
* Routing key: 路由键
* Bindings: 指定Exchange 和Queue绑定关系

### 传输层架构

``二进制协议，消息被组织成数据帧，有很多类型， 数据帧携带协议方法和其他信息， 所有数据帧都有基本的格式: 帧头, 负载, 帧尾， 负载的格式依赖于数据帧的类型``

#### AMQP数据类型

* Integers(1-8的十进制数字)用于表示大小，数量，限制等， 整数类型无符号的， 可以在帧内不对齐
* Bits(统一为8个字节): 用于标识开、关值
* short Strings: 保存简单的文本属性 8个字节
* Long Strings: 用于保存二进制数据块
* Filed tables: 键值对

#### 协议协商

客户端和服务端要进行协议协商， 服务端向客户端提出一些选项， 客户端能接受或修改， 都认同协商结果，继续进行连接的建立过程。

* 真实的协议和版本
* 加密参数和认证部分
* 数据帧的最大大小， 通道数量以及其他操作限制

#### 数据帧界定

* 每个连接发送单一数据帧， 简单但是慢
* 在流中添加帧的边界， 简单但是解析慢
* 计算数据帧的大小， 在每个数据帧头部加上大小， 简单快速，AMQP的选择

### 分层架构

   * Module Layer: 协议最上层，定义了一些供客户端调用的命令, 客户端可以利用这些命令实现自己的业务逻辑
   * Session Layer: 中间层, 将客户端的命令发送到服务器，再将服务器的应答返回给客户端， 主要为客户端与服务器之间的通信提供可靠性同步机制和错误处理
   * Transport layer: 最底层, 传输二进制数据流，提供帧的处理，信道复用，错误检测，数据表示等。
