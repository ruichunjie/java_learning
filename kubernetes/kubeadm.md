## 部署

1. 安装docker

```shell

  yum -y install docker
  systemctl enable docker
  systemctl start docker
  
```

2. 禁用SELinux

```shell
sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/sysconfig/selinux
setenforece 0
```

3.禁用swap分区
```shell
swapoff -a 
vim /etc/fstab
#/dev/mapper/centos-swap swap ...
```

4.修改防火墙规则
docker 从1.13版本调整了默认防火墙规则， 禁用了iptables的filter表中的FORWARD链, 这样会引起跨节点的Pod无法通信， 需要修改规则
```shell
iptables -P FORWARD ACCEPT
iptables-save
```

5. 添加软件仓库
```shell
vim /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enable=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
yum install -y kubeadm
``` 

6.安装kubelet

```shell
 yum install -y kubelet kubeadm kubectl ipvsadm
```

7.配置转发
```shell
vim /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables=1
net.bridge.bridge-nf-call-iptables=1
vm.swappiness=0
sysctl --system
```

8.修改kubelet配置文件, 添加阿里镜像
```shell
vim /etc/sysconfig/kubelet
KUBELET_EXTRA_ARGS= --cgfroup-driver=systemd --pod-infra-container-image=registry.cn-hangzhou.aliyuncs.com/google_containers/pau
se-amd64:3.1

```
重新启动kubelet
```shell
systemctl daemon-reload
systemctl enable kubelet && systemctl restart kubelet
```

9. 部署master节点

```shell
kubeadm init
```

此处可能出现问题
![](/assets/kubeadm_master.png)

一一解决
https://www.ywcsb.vip/blog/94.html
```shell
docker info | grep Cgroup
vim /usr/lib/systemd/system/docker.service
ExecStart=/usr/bin/dockerd --exec-opt native.cgroupdriver=systemd

systemctl daemon-reload
systemctl restart docker

docker pull coredns/coredns:1.8.0
docker tag coredns/coredns:1.8.0 registry.aliyuncs.com/google_containers/coredns:v1.8.0
```
此处可能出现问题
```shell
```

kubeadm init --image-repository=registry.aliyuncs.com/google_containers
