

## 部署

```shell
 rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
## 在/etc/yum.repos.d/目录下，新建文件logstash.repo，内容如下：
[logstash-7.x]
name=Elastic repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md

sudo yum install  -y logstash
```

/usr/share/logstash/bin