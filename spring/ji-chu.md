# Spring

## 优势

方便解耦， 简化开发

AOP编程

声明式事务

方便测试

方便集成框架

降低API开发难度

源码范例

## 架构

![](/assets/spring.png)

## IOC

> 什么是IOC？

IOC Inversion of Control\(控制反转/反转控制\)  
Java开发领域对象的创建,管理的问题  
解决了对象之间的耦合问题

JavaSE: ApplicationContext appicationContext = new ClassPathXmlApplicationContext\("beans.xml"\);  
new FileSystemXmlApplicationContext\("c:/beans.xml"\);  
ApplicationContext applicationContext = new AnnotationConfigApplicationContext\(SpringConfig.class\)

Java Web: ContextLoaderListener\(监听器加载xml\)  
  ContextLoaderListener\(监听器去加载加载配置类\)

> DI

控制权交给外部环境\(Spring环境\)

> IOC和DI

IOC 和DI描述的是同一件事情\(对象实例化及依赖关系的维护\)， 只不过角度不同。

IOC站在对象的角度， 对象实例化及管理的权利交给了容器

DI站在容器的角度， 容器会把对象依赖的其他对象注入

## AOP

> 定义

Aspect oriented Programming 面向切面编程

## 碎片知识点

> BeanFactoryRegistryPostProcessor

触发时机是BeanDefinition加载完毕，Bean还没有初始化

> BeanDifinitionRegistryPostProcessor

继承了BeanFactoryRegistryPostProcessor,触发时机是BeanDefinition还没定义

## Bean生命周期

![](/assets/lifeCycle.png)

## 简化开发四个策略

* 基于POJO的轻量级和最小侵入性编程
* 通过依赖注入和面向接口松耦合
* 基于切面和惯性进行声明式编程
* 基于切面和模板减少样板式代码

## 注解组件

@ComponentScan\(value="", includeFilters={@Filter\(type= FilterType.ANNOTATION, value = Controller.class\)}, useDefaultFilters =false\)

@Scope  
@lazy

@Import  
ImportSelect  
ImportBeanDefinitionRegistrar

FactoryBean

@Value\("\#{8-5}"\)

@PropertySource\("classPath:..yml"\)

## 九大组件

HandlerMapping

HandlerAdapter

HandlerExceptionResolver

ViewResolver

RequestToViewNameTranslator

LocaleResolver

ThemeResolver

MultipartResolver

FlashMapManager

&lt;mvc:default-servlet-handler/&gt;

添加标签后， 会在SpringMVC 上下文中定义一个DefaultServletHttpRequestHandler对象， 这个对象会对进入DispatcherServlet的url请求进行过滤筛查， 如果是一个静态资源请求， 会将请求转由tomcat默认的DefaultServelet处理， 但这种配置在html放在resources, WEB-INF下不生效

&lt;mvc: resources location= "classpath: /" mapping="/resources/\*\*"/&gt;

ModelMap Model Map 类是 BindingAwareModelMap

定义日期类型转换器

```
public class DateConverter implements Converter<String, Date> {

    public Date convert(String, source){
       //...
        return sdf.parse(source);
    }
}
```

&lt;bean class="org.springframework.format.support.FormattingConversionServiceFactoryBean&gt;

&lt;property name="converters"&gt;

&lt;set&gt;

&lt;bean class =""/&gt;

&lt;/set&gt;

&lt;property&gt;

&lt;bean/&gt;

&lt;mvc:annotation-driver conversion-service=""/&gt;

乱码配置过滤器

&lt;filter&gt;

&lt;filter-name&gt;encoding&lt;filter-name/&gt;

&lt;filter-class&gt;org.springframework.web.filter.CharacterEncodingFilter&lt;/filter-class&gt;

&lt;init-param&gt;

```
 &lt;param-name&gt;encoding&lt;/param-name&gt;

&lt;param-value&gt;UTF-8&lt;/param-value&gt;
```

&lt;/init-param&gt;

&lt;/filter&gt;

&lt;filter-mapping&gt;

&lt;filter-name&gt;encoding&lt;/filter-name&gt;

&lt;url-pattern&gt; /\*&lt;/url-pattern&gt;

&lt;filter-mapping&gt;

上面的是POST请求出现乱码

如果是GET请求

&lt;Connector URIEncoding="utf-8" connectTimeout="200000"  port="80"  protocol ="HTTP/1.1" redirectPort="8443"/&gt;

SpringMVC 转换过滤器 请求参数中是否有\_method，如果有按指定的请求方式进行转换

&lt;filter&gt;

&lt;filter-name&gt;hiddenHttpMethodFilter&lt;filter-name/&gt;

&lt;filter-class&gt;org.springframework.web.filter.hiddenHttpMethodFilter&lt;/filter-class&gt;

&lt;init-param&gt;

&lt;filter-mapping&gt;

&lt;filter-name&gt;hiddenHttpMethodFilter&lt;/filter-name&gt;

&lt;url-pattern&gt; /\*&lt;/url-pattern&gt;

&lt;filter-mapping&gt;

&lt;form method='post' enctype= "multipart/form-data&gt;&lt;/form&gt;





RedirectAttributes rediretAttributes.addFlashAttribute\("name", name\);


SpringBoot 和SPring Cloud版本  https://start.spring.io/actuator/info
