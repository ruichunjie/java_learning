## 基础应用

### String
   Redis 字符串是动态字符串, 是可以修改的字符串, 内部结构实现类似ArrayList, 采取预分配减少内存的频繁分配， 当字符串长度小于1M 扩容是加倍现有的空间, 如果超过1M， 扩容一次只会多扩1M的空间， 字符串最大长度是512M

```shell
 set name code
 get name
 exists name
 del name
 mset name1 boy name2 girl name3 unknown
 mget name1 name2 name3
 expire name 5
 setex name 5 code
 setnx name code
 set age 10 
 incr age
 incrby age 5
 ttl age
```

### list
  类似LinkedList, 是链表不是数组， 插入删除很快 索引定位慢,可以用作异步队列使用, 使用双向指针顺序， 支持向前向后遍历， 当列表弹出最后一个元素， 数据结构被删除， 内存被回收, 
底层是quicklist('快速链表')， 在列表元素较少时使用连续的内存存储也就是ziplist (压缩列表),  redis 将ziplist 和链表结合使用双向指针穿起来， 既满足了快速插入性能，又不会出现太大的空间浪费
```shell
   rpush books 1 2 3
   llen books  -------(这边注意没有rlen命令)
   lpop books
   rpop books
   lpush books 1
   --- 比较慢的操作
   lindex books 0  -- O(n)
   lrange books 0 -1  -- O(n) 获取所有
   ltrim books 1 -1  --O(n)  从1开始截取后面的
   ltrim books 1 0 -- 清空了整个列表
   blpop books 10  -- 阻塞取 等待10s 读不到返回  10 是必须有的 不然会报错 为0阻塞读
   brpop books 10  -- 阻塞取 等待10s 读不到返回  10 是必须有的 不然会报错 为0阻塞读

```

### hash
   类似于HashMap, 无序字典， 实现结构也是数组+链表，不同的是redis值只能是字符串， rehash是渐进式的rehash策略， 与字符串对比，可以对结构中的每个字段单独存储， 缺点是消耗要大于单个字符串

```shell
  hset books java "think in"
  hgetall books
  hlen books
  hget books java
  hmset books java 123 pythin 322
  hincrby books java 1 --- 没有hincr
  hdel books java
```

### set
   类似HashSet, 内部键值对是无序的， 唯一的， 内部实现是特殊的字典， 所有的value 是NULL, 当集合最后一个元素被移除之后， 数据结构被自动删除， 内存被回收
```shell
    sadd books python
    sadd books java golang
    smembers books
    sismember books java
    scard books
    spop books
```

### zset
   类似SortedSet和HashMap的结合体, 内部实现用的是跳表， 最后一个元素被移除后， 数据结构自动删除，内存被回收, 跳表内部采取一个随机策略来决定元素在第几层
···shell
   zadd books 10 java -- 在这里可以有相同score的key
   zrange books 0 -1
   zrevrange books 0 -1
   zcard books
   zscore books java
   zrank books java -- 从零开始
   zrangeby score books -inf 10 withscores
   zrem books java
```

> 容器基础通用规则

list hash set zset容器共享两条规则 create if not exists; drop if no elements
过期时间是以对象为单位， hash结构的过期是整个hash对象的过期, 如果已经设置过了过期时间，然后set修改后 过期时间会消失 

> 延时队列
 redis 消息队列没有ack保证 可靠性不高
 
### 位图
 设置的时候有零存整取 零存零取 整存整取
``` shell
   setbit s 1 1  --零存
   getbit s 1 -- 零取
   set w h -- 整存
   get w -- 整取
```
 > 统计查找 bitcount bitpos
  bitcount bitpos这两个命令 start 和end参数是字节索引，指定的位范围必须是8的倍数
```shell
  set w hello
  bitcount w
  bitcount w 0 0 -- 第一个字符串 1的位数
  bitcount w 0 1 -- 前两个字符串 1的位数
  bitpos  w 0 -- 第一个0位
  bitpos w 1 -- 第一个1位
  bitpos w 1 1 1 从第二个字符串开始 第一个1位
  bitpos w 1 2 2 从第三个字符串开始 第一个1位
```
  魔数指令bitfield
  bitfield 有三个指令 分别是get set incrby  
```shell
   set w hello 
   bitfield w get u4 0 --从第一个位取4个位， 结果是无符号数[u]
   bitfield w get i4 0 --从第一个位取4个位， 结果是有符号数[i]
```
### HyperLogLog
pfadd
pfcount 
pfmerge
在计数比较小时，采用系数矩阵存储, 空间占用很小， 在计数慢慢变大，系数矩阵占用空间渐渐超过了阈值， 才会一次性转变成稠密矩阵， 占用12KB
2的14次方 *6 /8 = 12KB 

### Bloom Filter

bf.add
bf.exists
bf.madd
bf.mexists
bf.reserve 三个参数 key error_rate initial_size

### GeoHash
geoadd company 116 39 meituan
geodist company juejin xiaomi km 
geopos

### scan
scan 0 match key99* count 1000

redis-cli -a redistest -n 2 --scan --pattern 'code:dispatchCode:1610202103*' | xargs redis-cli -a redistest -n 2  del
批量删除数据


在redis中所有的key都存储在一个很大的字典中， 结构和HashMap 一样， 是一维数组，二维链表结构， 第一维数组大小总是2的n次方, 扩容一次数组 大小空间加倍
scan 返回的游标就是一维数组的位置索引(槽), 采用了高位进位加法， 在扩容和缩容时避免了槽位的遍历重复和遗漏
