## 命令

> 创建用户

```shell
   passwd
   useradd cliu
   cat /etc/passwd
   cat /etc/group
```

> 浏览文件

```shell
    ls -l
    
```

![](/assets/ls.png)
 
   第一个字段的第一个字符是文件类型,如果是'-', 表示普通文件， 如果是d, 表示目录
   第一个字符剩下的9个字符是模式，其实是权限位， 3个一组, 每组rwx表示 read, write, execute。
   这三组分别代表文件所属的用户权限, 文件所属的组权限， 以及其他用户的权限, 如果要改变权限， 可以使用chmod 711 hosts
   r =4, w =2, x =1
   chown 更改文件拥有者 chgrp改变所属组 
   第二个字段是硬链接数目
   第三个字段是所属用户
   第四个字段是所属组
   第五个字段是文件的大小
   第六个字段是文件被修改的日期
   最后是文件名
   
   
> 安装软件

   下载rpm 或者deb, linux有两大体系，一种是CentOS体系， 一个是Ubuntu体系， 前者用rpm， 后者用deb
   rpm -i jdk-XXX_linux-x64_bin.rpm
   dpkg -i jdk-XXX_linux-x64_bin.deb
   
   可以用rpm -qa和dpkg -l, -q 就是query, a 是all, -l 是list
   搜索工具grep
   rpm -qa |grep jdk
   很长的结果分页展示
   rpm -qa |more 和 rpm -qa |less
   more是分页后只能往后翻页， 放到最后一页自动结束返回命令行， less向前向后都能翻页， 输入q返回命令行， q就是quit
   用空格能向后翻页 b向前翻页 u 向前翻半页 d向后翻半页
   rpm -e 和dpkg -r 删除
   
   Linux的软件管家， Centos是yum, Ubuntu下面是apt-get
   yum install java-11-openjdk.x86_64
   apt-get install openjdk-9-jdk
   yum erase java-11-openjdk.x86_64
   apt-get purge openjdk-9-jdk
   
   Linux允许配置从哪里下载软件， centos配置文件在/etc/yum.repos.d/CentOS-Base.repo
   ```shell
   
   [base]
   name=CentOS-$releasever - Base - 163.com
   baseurl=http://mirrors.163.com/centos/$releasever/os/$basearch/
   gpgcheck=1
   gpgkey=http://mirrors.163.com/centos/RPM-GPG-KEY-CentOS-7
   
   ```
   
   Ubuntu配置文件再/etc/apt/sources.list
   
   ```shell
   
deb http://mirrors.163.com/ubuntu/ xenial main restricted universe multiverse
deb http://mirrors.163.com/ubuntu/ xenial-security main restricted universe multiverse
deb http://mirrors.163.com/ubuntu/ xenial-updates main restricted universe multiverse
deb http://mirrors.163.com/ubuntu/ xenial-proposed main restricted universe multiverse
deb http://mirrors.163.com/ubuntu/ xenial-backports main restricted universe multiverse
   
   ```
   
   Linux的主执行文件会放在 /usr/bin 或者 /usr/sbin 下面，其他的库文件会放在 /var 下面，配置文件会放在 /etc 下面
   
   将安装好的路径直接下载下来，然后解压缩成为一个整的路径,Linux 上面有一个工具 wget，后面加上链接，就能从网上下载了
   
   tar xvzf jdk-XXX_linux-x64_bin.tar.gz
   tar 解压缩后要配置环境变量
   ```shell
     
export JAVA_HOME=/root/jdk-XXX_linux-x64
export PATH=$JAVA_HOME/bin:$PATH
   ```
   
   在当前用户的默认工作目录，例如 /root 或者 /home/cliu8 下面，有一个.bashrc 文件，这个文件是以点开头的，这个文件默认看不到，需要 ls -la 才能看到，a 就是 all。每次登录的时候，这个文件都会运行，因而把它放在这里。这样登录进来就会自动执行。当然也可以通过 source .bashrc 手动执行。
   
   
> 运行程序

Linux 不是根据后缀名来执行的。它的执行条件是这样的：只要文件有 x 执行权限，都能到文件所在的目录下，通过./filename运行这个程序。当然，如果放在 PATH 里设置的路径下面，就不用./ 了，直接输入文件名就可以运行了，Linux 会帮你找。

   后台运行 nohup命令
   最终命令的一般形式为nohup command >out.file 2>&1 &。这里面，“1”表示文件描述符 1，表示标准输出，“2”表示文件描述符2，意思是标准错误输出，“2>&1”表示标准输出和错误输出合并了。合并到哪里去呢？到 out.file 里。
   ps -ef |grep 关键字  |awk '{print $2}'|xargs kill -9
   awk 工具可以很灵活地对文本进行处理，这里的 awk '{print $2}'是指第二列的内容，是运行的程序 ID。我们可以通过 xargs 传递给 kill -9，也就是发给这个运行的程序一个信号，让它关闭。如果你已经知道运行的程序 ID，可以直接使用 kill 关闭运行的程序。
   
   以服务的方式运行
   systemctl start mysql启动 MySQL，通过systemctl enable mysql设置开机启动。之所以成为服务并且能够开机启动，是因为在 /lib/systemd/system 目录下会创建一个 XXX.service 的配置文件，里面定义了如何启动、如何关闭。
   在 CentOS 里有些特殊，MySQL 被 Oracle 收购后，因为担心授权问题，改为使用 MariaDB，它是 MySQL 的一个分支。通过命令yum install mariadb-server mariadb进行安装，命令systemctl start mariadb启动，命令systemctl enable mariadb设置开机启动。同理，会在 /usr/lib/systemd/system 目录下，创建一个 XXX.service 的配置文件，从而成为一个服务。
   shutdown -h now是现在就关机，reboot就是重启。


