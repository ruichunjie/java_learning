## 原型模式

> 定义

​      Prototype Pattern, 原型实例指定创建对象的种类，并且通过拷贝这些原型创建新的对象,调用者不需要知道任何创建细节，不调用构造函数，属于创建型模式

> 适用场景

     -  类初始化消耗资源较多
     -  new 产生的一个对象需要非常繁琐的过程(数据准备， 访问权限等)
     -  构造函数比较复杂
     -  循环体中生产大量对象

> 类别

* 浅克隆
* 深克隆
```java
   ByteArrayOutputStream bos = new ByteArrayOutputStream();
   ObjectOutputStream oos = new ObjectOutputStream(bos);
   oos.writeObject(this);
   ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
   ObjectInputStream ois = new ObjectInputStream(bis);
   return (T) ois.readObject(); 
```

> 优点

* 性能优良， Java自带的原型模式是基于内存二进制流的拷贝，比直接new一个对象性能上提升很多
* 可以使用深克隆方式保存对象的状态，使用原型模式将对象复制一份并将其状态保存起来，简化了创建对象

> 缺点

* 必须配备克隆(或可拷贝)方法
* 当对已有类进行改造的时候，需要修改代码，违背开闭