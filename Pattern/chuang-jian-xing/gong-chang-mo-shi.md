## 工厂模式

### 简单工厂(Simple Factory)

> 定义  

   由一个工厂对象决定创建出哪一种产品类的实例，属于创建型模式， 但不属于GOF23种设计模式。一般可以考虑和单例模式结合使用。

> 适用场景  

   工厂类负责创建的对象很少, 客户端只需要传入工厂类的参数， 对于如何创建对象的逻辑并不关心。

> 优点   

   只需要传入一个正确的参数， 就可以获取需要的对象，无须知道创建细节

> 缺点   

   工厂类职责过重，增加新的产品时要修改工厂类的判断逻辑，违背开闭原则，不易于扩展过于复杂的产品结构

> 源码案例

 java.text.DateFormat
```java
      
     public final StringBuffer format(Object obj, StringBuffer toAppendTo,  FieldPosition fieldPosition) {
        if (obj instanceof Date)
            return format( (Date)obj, toAppendTo, fieldPosition );
        else if (obj instanceof Number)
            return format( new Date(((Number)obj).longValue()),
                          toAppendTo, fieldPosition );
        else
            throw new IllegalArgumentException("Cannot format given Object as a Date");
    }

```

java.util.Calendar
```java
  public static Calendar getInstance(TimeZone zone,
                                       Locale aLocale)
    {
        return createCalendar(zone, aLocale);
    }

    private static Calendar createCalendar(TimeZone zone,
                                           Locale aLocale)
    {
        CalendarProvider provider =
            LocaleProviderAdapter.getAdapter(CalendarProvider.class, aLocale)
                                 .getCalendarProvider();
        if (provider != null) {
            try {
                return provider.getInstance(zone, aLocale);
            } catch (IllegalArgumentException iae) {
                // fall back to the default instantiation
            }
        }

        Calendar cal = null;

        if (aLocale.hasExtensions()) {
            String caltype = aLocale.getUnicodeLocaleType("ca");
            if (caltype != null) {
                switch (caltype) {
                case "buddhist":
                cal = new BuddhistCalendar(zone, aLocale);
                    break;
                case "japanese":
                    cal = new JapaneseImperialCalendar(zone, aLocale);
                    break;
                case "gregory":
                    cal = new GregorianCalendar(zone, aLocale);
                    break;
                }
            }
        }
        if (cal == null) {
            // If no known calendar type is explicitly specified,
            // perform the traditional way to create a Calendar:
            // create a BuddhistCalendar for th_TH locale,
            // a JapaneseImperialCalendar for ja_JP_JP locale, or
            // a GregorianCalendar for any other locales.
            // NOTE: The language, country and variant strings are interned.
            if (aLocale.getLanguage() == "th" && aLocale.getCountry() == "TH") {
                cal = new BuddhistCalendar(zone, aLocale);
            } else if (aLocale.getVariant() == "JP" && aLocale.getLanguage() == "ja"
                       && aLocale.getCountry() == "JP") {
                cal = new JapaneseImperialCalendar(zone, aLocale);
            } else {
                cal = new GregorianCalendar(zone, aLocale);
            }
        }
        return cal;
    }
```

### 工厂方法(Factory Method Pattern)

> 定义

   定义一个创建对象的接口，但让实现这个接口的类来决定实例化哪个类，工厂方法让类的实例化推迟到子类进行, 属于创建型模式

> 适用场景

   创建对象需要大量重复的代码， 客户端不依赖于产品类实例如何被创建，实现等细节。一个类通过其子类来指定创建哪个对象。一个类要组合其他类对象，做初始化操作，将复杂的创建逻辑拆分到多个工厂类中。

> 优点

   用户只需关心所需产品对应的工厂， 无须关心创建细节。加入新产品符合开闭原则，提高了可扩展性

> 缺点

   类的个数容易过多， 增加了代码结构的复杂度， 增加了系统的抽象性和理解难度

### 抽象工厂模式(Abstract Factory Pattern)

> 定义

   提供一个创建一系列相关或相互依赖对象的接口，无须指定他们具体的类，创建型模式

> 使用场景

   客户端不依赖于产品类实例如何被创建, 实现等细节，强调一系列相关的产品对象(同一个产品族)一起使用创建对象需要大量重复的代码。提供一个产品类的库，所有的产品以同样的接口出现，从而使客户端不依赖于具体实现。
   产品族: 一些列的相关的产品，整合到一起有关联性
   产品等级: 同一个继承体系

> 优点

   具体产品在应用层代码隔离， 无须关心创建细节， 将一个系列的产品族统一到一起创建

> 缺点

   规定了所有可能被创建的产品集合， 产品族中创建新的产品困难，需要修改抽象工厂的接口，增加了系统的抽象和理解难度。