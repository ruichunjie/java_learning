## 桥接模式



> 定义

​     Bridge Pattern, 桥梁模式， 接口模式， 柄体模式，是将抽象部分与它的具体实现部分分离， 使得它们可以独立的变化，属于结构型模式,  通过组合的方式建立两个类之间的联系，而不是继承，类似于多重继承方案， 但多重继承违背了单一职责，复用性差， 核心在于解耦抽象和实现



> 构成

​    抽象:   持有一个对实现角色的引用， 抽象角色中的方法需要实现角色来实现，抽象角色一般是抽象类

​    修正抽象： 抽象的具体实现， 对抽象的方法进行完善和扩展

​    实现： 确定实现维度的基本操作， 提供给抽象使用，一般是接口或者抽象类

​    具体实现： 实现的具体实现



> 应用场景

​    当一个类内部具备两种或多种变化维度时， 使用桥接模式可以解耦变化的维度，使高层代码结构稳定，桥接模式适用于

      *   在抽象和具体实现之间需要增加更多的灵活性
      *   一个类存在两个(或多个)独立变化的维度， 而这些维度需要独立进行扩展
      *   不希望使用继承， 或多层继承导致系统类的个数剧增



> 源码应用

DriverManager



> 优点

    *   分离抽象部分及具体实现部分
    *   提高系统的扩展性
    *   符合开闭原则
    *   符合合成复用原则



> 缺点‘

*  增加了系统的理解与设计难度
* 正确识别系统中两个独立变化的维度