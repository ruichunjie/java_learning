## 单例模式

> 定义

 Singleton Pattern, 确保一个类在任何情况下都绝对只有一个实例，并提供一个全局访问点,隐藏其所有的构造方法，属于创建型模式

> 适用场景

 确保任何情况下都只有一个实例

> 常见写法

* 饿汉式单例
    
    - 在单例类首次加载时创建实例
    
    -  执行效率高,性能高，无锁，可能造成内存浪费
    
      ```java
      public class A{
          private static final A a = new A();
          private A(){}
          public static A getInstance(){return a;}
      }
      ```
    
      
    
* 懒汉式单例

       - 被外部类调用时创建实例
       - 节省内存，线程安全(第一个线程不安全)， 可读性不高， 不够优雅


      ```java
    public class A{
        private A（）{}
        private static A a;
        public synchronized static A getInstance(){
            if(Objects.isNull(a)){
                a = new A();
            }
            return a;
        }
    }
    
    public class A{
        private A（）{}
        private volatile static A a;
        public static A getInstance(){
            if(Objects.isNull(a)){
                synchronized(A.class){
                    if(Objects.isNull(a)){
                       a = new A(); 
                    }
                }
            }
            return a;
        }
    }
      ```

    - 静态内部类

      - 写法优雅，利用JAVA本身语法特点, 性能高，避免内存浪费  



      ```java
      public class A{
          private A(){}
          private static A getInstance(){
              return LazyHolder.INSTANCE;
          }
          
          private static class LazyHolder{
              private static final A INSTANCE = new A();
          }
      }
      ```

      

* 注册式单例

     - 将每个实例都缓存到统一容器中，使用唯一标识获取实例

       - 枚举相当于实现了Enum类，反射创建枚举会报异常，耗费内存

       - 容器单例

         ```java
         public class A {
             private A (){}
             private static Map<String, Object> ioc = new ConcurrentHashMap();
             
             public synchronized static Object getInstance(String className) throws Exception{
                 Object instance = null;
                 if(ioc.containsKey(className)){
                     instance = Class.forName(className).newInstance();
                 }
                 return instance;
             }
         }
         ```

         

* ThreadLocal 单例
  		-   保证线程内部的全局唯一， 且天生线程安全

> 破坏单例

   * 反射破坏

   * 序列化破坏

     ```java
     A a1 = null
     A a2 = A.getInstance();
     FileOutputStream fos = new FileOutputStream("obj");
     ObjectOutputStream oos = new ObjectOutputSteam（fos);
     oos.writeObject(obj);
     oos.flush();
     oos.close();
     
     FileInputStream fis = new FileInputStream("obj");
     ObjectInputStream ois = new ObjectInputStream(fis);
     s1 = (A)ois.readObject();
     ois.close();
     ```

     对策:

      利用桥接模式，编写readResolve方法

     ```java 
     public A readResolve(){
         return instance;
     }
     ```

     原理在readObject方法底层

     ```java
     
     if (obj != null &&
         handles.lookupException(passHandle) == null &&
         desc.hasReadResolveMethod())
     {
         Object rep = desc.invokeReadResolve(obj);
         if (unshared && rep.getClass().isArray()) {
             rep = cloneArray(rep);
         }
         if (rep != obj) {
             // Filter the replacement object
             if (rep != null) {
                 if (rep.getClass().isArray()) {
                     filterCheck(rep.getClass(), Array.getLength(rep));
                 } else {
                     filterCheck(rep.getClass(), -1);
                 }
             }
             handles.setObject(passHandle, obj = rep);
         }
     }
     
     ```

     

> 优点

- 在内存中只有一个实例， 减少了内存开销
- 可以避免对资源的多重占用
- 设置全局访问点，严格控制访问

> 缺点

- 没有接口，扩展困难

> 案例

ServletContext

ServletConfig

ApplicationContext

DBPool
