## 代理模式

> 定义

​      Proxy Pattern, 为其他对象提供一种代理， 以控制对这个对象的访问，代理对象在客户端和目标对象之间起到中介作用， 属于结构型设计模式

> 适用场景

​     保护目标对象

​     增强目标对象

> 类别

* 静态代理  显式声明被代理对象
* 动态代理   动态配置和替换被代理对象
  - 实现原理
    * 拿到代理类的引用， 并且获取所有的接口(反射获取)
    * JDK Proxy类重新生成一个新的类， 实现被代理类所有接口的方法
    * 动态生成Java代码，把增强逻辑加入新生成代码中
    * 编译生成新的Java代码的class文件
    * 加载并重新运行新的class， 得到的类就是全新类
  - CGLib和JDK动态代理对比
    * JDK动态代理实现了被代理对象的接口， CGLib继承了被代理对象
    * JDK和CGLib都是在运行期生成字节码，JDK直接写Class字节码， CGLib使用ASM框架写Class字节码，Cglib代理实现更复杂，生成代理类比JDK效率低
    * JDK调用代理方法， 通过反射机制调用， CGLib通过FastClass机制直接调用方法， CGLib执行效率高
    * CGLib无法代理final修饰的方法

> 优点

* 代理模式能将代理对象与真实被调用的目标对象分离
* 一定程度上降低了系统的耦合程度， 易于扩展
* 代理可以起到保护目标对象的作用
* 增强目标对象的职责

> 缺点

* 代理模式会造成系统设计中类的数目增加
* 在客户端和目标对象之间增加了一个代理对象， 请求处理速度变慢， 增加了系统的复杂度

> Spring 代理原则

* 当Bean有实现接口，用JDK动态代理
* 当Bean没有实现接口， 选择CGLib
* 通过配置强制使用CGLib

```xml
<aop:aspectj-autoproxy  proxy-tartget-class="true"/>
```

> 自定义代理

```java

public interface A {
    void insertData();
}

package com.tn.mes.learn;

import com.tn.mes.common.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Objects;

public class CustomClassLoader extends ClassLoader  {

    private File classPathFile;
    public CustomClassLoader(){
        String classPath = CustomClassLoader.class.getResource("").getPath();
        this.classPathFile = new File(classPath);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {

        String className = CustomClassLoader.class.getPackage().getName()+ "."+name;
        if(Objects.nonNull(classPathFile)){
            File classFile = new File(classPathFile, name.replaceAll("\\.", "/")+".class");
            if(classFile.exists()){
                FileInputStream in = null;
                ByteArrayOutputStream out = null;
                try{
                    in = new FileInputStream(classFile);
                    out = new ByteArrayOutputStream();
                    byte [] buff = new byte[1024];
                    int len;
                    while ((len = in.read(buff)) != -1){
                        out.write(buff,0,len);
                    }
                    return defineClass(className,out.toByteArray(),0,out.size());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}

package com.tn.mes.learn;

import java.lang.reflect.Method;

public interface CustomInvocationHandler {

    Object invoke(Object proxy, Method method, Object[]args) throws Throwable;
}


package com.tn.mes.learn;

import com.tn.mes.qualitycontrol.process.service.IQualityResultDetailService;

import javax.tools.JavaCompiler;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class CustomProxy {

    public static final String ln = "\r\n";

    public static Object newProxyInstance(CustomClassLoader classLoader, Class<?>[] interfaces,
                                          CustomInvocationHandler h) throws Exception {

        //step1  动态生成源代码 .java文件
        String src = generateSrc(interfaces);

        //step2  Java文件输出磁盘
        String filePath = CustomProxy.class.getResource("").getPath();
        File f = new File(filePath + "$Proxy0.java");
        FileWriter fw = new FileWriter(f);
        fw.write(src);
        fw.flush();
        fw.close();

        //step3
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager manager = compiler.getStandardFileManager(null, null, null);
        Iterable iterable = manager.getJavaFileObjects(f);

        JavaCompiler.CompilationTask task =  compiler.getTask(null, manager, null, null, null, iterable);
        task.call();
        manager.close();

        Class proxyClass = classLoader.findClass("$Proxy0");
        Constructor c = proxyClass.getConstructor(CustomInvocationHandler.class);
//        f.delete();
        return c.newInstance(h);
    }

    /**
     * 生成源代码
     *
     * @param interfaces
     * @return
     */
    private static String generateSrc(Class<?>[] interfaces) {

        StringBuilder sb = new StringBuilder()
                .append(CustomProxy.class.getPackage() + ";" + ln)
                .append("import " + interfaces[0].getName() + ";" + ln)
                .append("import java.lang.reflect.*;" + ln)
                .append("public class $Proxy0 implements "+ interfaces[0].getName() + "{" +ln)
                .append("CustomInvocationHandler h;" +ln)
                .append("public $Proxy0(CustomInvocationHandler h) {"+ln)
                .append("this.h = h;"+ln)
                .append("}"+ln);
        for(Method m: interfaces[0].getMethods()){
            Class<?>[] params= m.getParameterTypes();

            StringBuilder paramNames = new StringBuilder();
            StringBuilder paramValues = new StringBuilder();
            StringBuilder paramClasses = new StringBuilder();

            for(int i=0; i< params.length; i++){
                Class clazz = params[i];
                String type = clazz.getName();
                String paramName = toLowerFirstCase(clazz.getSimpleName());

                paramNames.append(type + " " + paramName);
                paramValues.append(paramName);
                paramClasses.append(clazz.getName() +".class");
                if( i<params.length-1){
                    paramNames.append(",");
                    paramClasses.append(",");
                    paramValues.append(",");
                }
            }

            sb.append("public "+ m.getReturnType().getName() + "  " + m.getName() + "(" + paramNames.toString() +"){" +ln)
                    .append("try{"+ln)
            .append("Method m = " + interfaces[0].getName() + ".class.getMethod(\"" + m.getName() + "\",new Class[]{" + paramClasses.toString() + "});" + ln)
            .append((hasReturnValue(m.getReturnType()) ? "return " : "") + getCaseCode("this.h.invoke(this,m,new Object[]{" + paramValues + "})",m.getReturnType()) + ";" + ln)
            .append("}catch(Error _ex) { }")
            .append("catch(Throwable e){" + ln)
            .append("throw new UndeclaredThrowableException(e);" + ln)
            .append("}")
            .append(getReturnEmptyCode(m.getReturnType()))
            .append("}");
        }
        sb.append("}" + ln);
        return sb.toString();
    }

    private static String getReturnEmptyCode(Class<?> returnClass){
        if(mappings.containsKey(returnClass)){
            return "return 0;";
        }else if(returnClass == void.class){
            return "";
        }else {
            return "return null;";
        }
    }

    private static boolean hasReturnValue(Class<?> clazz){
        return clazz != void.class;
    }

    private static String toLowerFirstCase(String src){
        char [] chars = src.toCharArray();
        chars[0] += 32;
        return String.valueOf(chars);
    }
    private static Map<Class,Class> mappings = new HashMap<Class, Class>();
    static {
        mappings.put(int.class,Integer.class);
    }

    private static String getCaseCode(String code,Class<?> returnClass){
        if(mappings.containsKey(returnClass)){
            return "((" + mappings.get(returnClass).getName() +  ")" + code + ")." + returnClass.getSimpleName() + "Value()";
        }
        return code;
    }
}

class C implements CustomInvocationHandler{

    public Object obj;
    public Object getInstance(Object obj) throws Exception {
        this.obj =obj;
        return CustomProxy.newProxyInstance(new CustomClassLoader(), obj.getClass().getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return method.invoke(this.obj, args);
    }
}

class Test{

    public static void main(String[] args) throws Exception {
         A a =  (A)new C().getInstance(new B());
         a.insertData();
    }
}

class B implements A {

    @Override
    public void insertData() {
        System.out.println("q12");
    }
}


```

