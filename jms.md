`` JMS: Java消息服务(Java Message Service)应用程序接口，面向消息中间件(MOM, Message oriented Middleware)的API，用于两个程序之间，或分布式系统中发送消息，进行异步通信。``

#### JMS消息

消息是JMS中的一种类型对象， 由两部分组成: 报文头和消息主体。报文头包括消息头字段和消息头属性， 字段是JMS 协议规定的字段，属性可以由用户进行添加。

| 字段名称         | 含义                                                         |
| ---------------- | ------------------------------------------------------------ |
| JMSDestination   | 包含了消息要发送到的目地的                                   |
| JMSDeliveryMode  | 包含了消息在发送时指定的投递模式                             |
| JMSMessageID     | 包含了服务器发送的每个消息的唯一标识                         |
| JMSTimeStamp     | 包含了消息封装完成要发往服务器的时间, 不是真正向服务器发送的时间 |
| JMSCorrelationID | 客户端使用该字段的值与另一个消息关联，可能包含服务器规定的ID，应用指定字符串，服务器原生的byte[]值 |
| JMSReplyTo       | 包含了客户端发送消息的时候指定的Destination, 放期望的一个响应 |
| JMSRedelivered   | 如果为true, 告知消费者应用这条消息已经发送过了，消费者端应该小心别重复处理 |
| JMSType          | 消息发送的时候用于标识消息的类型，具体由JMS实现厂商决定      |
| JMSExpiration    | 到期时间， 发送消息时，send方法上指定的生存时间的值与当前GMT值 |
| JMSPriority      | 优先级，最低为0，最高为9。 0-4正常优先级， 5-9 快速优先级    |

消息主体带着应用程序的数据或负载， 分为几种类型:

1. 简单文本(TextMessage)
2. 可序列化对象(ObjectMessage)
3. 属性集合(MapMessage)
4. 字节流(BytesMessage)
5. 原始值流(StreamMessage)
6. 无有效负载消息(Message)



#### 体系结构

1. JMS供应商产品: JMS接口的实现
2. JMS Client: 生产或消费基于消息的java应用程序或对象
3. JMS Producer: 创建并发送消息的JMS客户
4. JMS Consumer:  接收消息的JMS客户
5. JMS Message: 在JMS客户之间传递的数据对象
6. JMS Queue: 缓存消息的容器
7. JMS Topic: Pub/Sub 模式



#### 对象模型

1. ConnectionFactory接口 连接工厂
2. Connection接口 连接
3. Destination接口 目标
4. Session接口 会话
5. MessageConsumer接口  消费者
6. MessageProducer接口 生产者
7. Message 接口 消息



#### 模式

1. 点对点
   * 一个消息只有一个消息获得
   * 生产者无需再接受者消费该消息期间处于运行状态，反之亦然
   * 每个成功处理的消息要么自动确认，要么由接收者手动确认
2. 发布/订阅
   * 支持向一个特定主题发布消息
   * 0/多个订阅者可能对接收特定消息主题的消息感兴趣
   * 消费者和发布者彼此不知道对方
   * 多个消费者可以获得消息

#### 传递方式

NON_PERSISTENT 消息最多投递一次， PERSISTENT 消息暂存后，再转发, 默认非持久性